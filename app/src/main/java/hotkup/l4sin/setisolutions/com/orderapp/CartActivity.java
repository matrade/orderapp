package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.Timestamp;
import java.util.ArrayList;

import hotkup.l4sin.setisolutions.com.orderapp.barcode.BarcodeCaptureActivity;

/**
 * Created by malicktraoredermane on 17-08-11.
 */

public class CartActivity extends AppCompatActivity {

    ArrayList<CartModel> CartModels;
    ListView cartList;
    Button orderButton;
    float totalPrice;
    TextView totalPriceView;
    TextView currency;
    Button backToMenu;
    public static final int CART_CHECK_EMPTY = 1;

    private static CartAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        cartList = (ListView) findViewById(R.id.list_cart);
        orderButton = (Button) findViewById(R.id.cart_order_button);
        totalPriceView = (TextView) findViewById(R.id.total_price);
        currency = (TextView) findViewById(R.id.food_currency);
        currency.setText(Variables.currency);

        TextView title = (TextView) findViewById(R.id.header_title);
        title.setText("Cart");

        totalPriceView.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (CartModels.size()==0){
                    setContentView(R.layout.activity_no_item_cart);
                    TextView headertitle = (TextView) findViewById(R.id.header_title);
                    headertitle.setText("Cart");
                    backToMenu = (Button) findViewById(R.id.cancel_cart);
                    backToMenu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                }
            }
        });
        CartModels= new ArrayList<>();

        orderButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent;

                if (!scanned()) {
                    Variables.qrcodeAccepted = false;
                    intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
                }
                else {
                    Variables.qrcodeAccepted = true;
                    intent = new Intent(getApplicationContext(), ConfirmOrderActivity.class);
                }
                startActivity(intent);
            }

        });

        updateActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Variables.cartContent.size() == 0){
            setContentView(R.layout.activity_no_item_cart);
            TextView headertitle = (TextView) findViewById(R.id.header_title);
            headertitle.setText("Cart");
            backToMenu = (Button) findViewById(R.id.cancel_cart);
            backToMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CART_CHECK_EMPTY) {
            if(Variables.cartContent.size() == 0){
                setContentView(R.layout.activity_no_item_cart);
                TextView headertitle = (TextView) findViewById(R.id.header_title);
                headertitle.setText("Cart");
                backToMenu = (Button) findViewById(R.id.cancel_cart);
                backToMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }
        }
    }
    boolean scanned(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        Timestamp timeFrozen = Variables.timestamp;

        if(timeFrozen != null){
            long diff = timestamp.getTime() - timeFrozen.getTime();
            return (diff>600);
        }

        return false;
    }

    void updateActivity(){
        ArrayList<Order> content = Variables.cartContent;
        totalPrice = 0;
        if(content.size() == 0){
            setContentView(R.layout.activity_no_item_cart);
            TextView headertitle = (TextView) findViewById(R.id.header_title);
            headertitle.setText("Cart");
            backToMenu = (Button) findViewById(R.id.cancel_cart);
            backToMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        for(Order contentItem : content){
            if (contentItem.getOption() != ""){
                CartModels.add(new CartModel(contentItem.getPlatName(),contentItem.getOption(),contentItem.platPrice,contentItem.getExtra()));
            }
            else {
                CartModels.add(new CartModel(contentItem.getPlatName(),contentItem.platPrice,contentItem.getExtra()));
            }

            totalPrice += contentItem.getPlatPrice();
        }

        totalPriceView.setText(String.format("%.2f",totalPrice));
        adapter= new CartAdapter(CartModels,getApplicationContext(),totalPriceView);

        cartList.setAdapter(adapter);
        cartList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //CartModel menuModel= CartModels.get(position);
                //Log.d("name",menuModel.getCartName());

            }
        });
    }


}