package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.api.CommonStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static hotkup.l4sin.setisolutions.com.orderapp.MenuDetailActivity.EXTRA_TAG_CODE;


/**
 * Created by malicktraoredermane on 17-09-30.
 */

public class MenuExtraActivity extends AppCompatActivity {

    ListView listExtra;

    Integer positionType;
    Integer positionMenu;

    String name;
    String price;

    Button addExtras;

    TextView cartContent;

    private static MenuExtraAdapter adapter;
    ArrayList<MenuExtraModel> MenuExtraModels;
    View cartButton;
    static final int PICK_CONTACT_REQUEST = 1;
    private static final String TAG = "Barcode-reader";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu_extra);

        addExtras = (Button) findViewById(R.id.add_extras);
        addExtras.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                addExtraToPrice();
            }
        });
        listExtra = (ListView) findViewById(R.id.list_extra);
        cartButton = (View) findViewById(R.id.cart_button_main);

        cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openCart();
            }
        });

        TextView headertitle = (TextView) findViewById(R.id.header_title);
        headertitle.setText("Select an extra");

        cartContent = (TextView) findViewById(R.id.cart_content);

        MenuExtraModels = new ArrayList<>();
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                positionType = null;
                positionMenu = null;
            } else {
                positionType = extras.getInt("type");
                positionMenu = extras.getInt("detail");
            }
        } else {
            positionType = (Integer) savedInstanceState.getSerializable("type");
            positionMenu = (Integer) savedInstanceState.getSerializable("detail");
        }
        new MenuExtraActivity.GetContacts().execute();

    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        cartContent.setText(String.valueOf(Variables.cartContent.size()));
    }

    private void addExtraToPrice(){
        Intent intent = new Intent();
        if(Variables.addOnList.size() > 0) {
            intent.putExtra(EXTRA_TAG_CODE, true);
        }
        else {
            intent.putExtra(EXTRA_TAG_CODE, false);
        }
        setResult(CommonStatusCodes.SUCCESS, intent);
        finish();
    }

    void openCart()
    {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        //Variables.addOnList.clear();
        finish();
    }
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            JSONObject menuList = Variables.menuRestaurant;
            try {
                JSONArray categories = menuList.getJSONArray("type");
                JSONObject type = categories.getJSONObject(positionType);
                JSONArray menuAll = type.getJSONArray("meal");
                JSONObject menu = menuAll.getJSONObject(positionMenu);

                name = menu.getString("name");
                price = menu.getString("price");

                Boolean addon_flag = menu.getBoolean("addon_flag");
                if(addon_flag){
                    JSONArray addons = menu.getJSONArray("addOn");
                    for (int i = 0; i < addons.length(); i++){
                        JSONObject addon = addons.optJSONObject(i);
                        String addonPrice = "";
                        String rawPrice = addon.getString("price");
                        float intPrice = Float.parseFloat(rawPrice);
                        if(intPrice > 0) {
                            addonPrice += rawPrice;
                        }

                        String addonName = addon.getString("name");
                        if(addonPrice == ""){
                            MenuExtraModels.add(new MenuExtraModel(addonName));
                        }
                        else{
                            MenuExtraModels.add(new MenuExtraModel(addonName,addonPrice));
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
                return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            adapter = new MenuExtraAdapter(MenuExtraModels, getApplicationContext());

            listExtra.setAdapter(adapter);

        }
    }

}
