package hotkup.l4sin.setisolutions.com.orderapp;

/**
 * Created by malicktraoredermane on 17-08-06.
 */

public class MenuModel {

    String menuName;
    String menuPrice;

    public MenuModel(String name, String price){
        menuName = name;
        menuPrice = price;
    }

    public String getMenuName() {
        return menuName;
    }

    public String getMenuPrice(){
        return menuPrice;
    }
}
