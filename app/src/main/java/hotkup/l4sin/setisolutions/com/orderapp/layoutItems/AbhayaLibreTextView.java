package hotkup.l4sin.setisolutions.com.orderapp.layoutItems;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by malicktraoredermane on 17-11-10.
 */

public class AbhayaLibreTextView extends android.support.v7.widget.AppCompatTextView {

    public AbhayaLibreTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AbhayaLibreTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AbhayaLibreTextView(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/AbhayaLibre-Regular.ttf");
        setTypeface(tf ,1);

    }
}
