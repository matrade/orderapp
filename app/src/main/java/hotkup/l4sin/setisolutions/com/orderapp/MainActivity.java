package hotkup.l4sin.setisolutions.com.orderapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final int PERMISSION_ACCESS_FINE_LOCATION = 1;
    private GoogleApiClient mGoogleApiClient;
    TextView questionPlace;
    TextView currentPlace;
    Button yesButton;
    Button noButton;
    TextView selectPlace;
    ListView choiceView;
    LocationManager locationManager ;
    boolean GpsStatus ;
    ArrayList<ListPlaceModel> ListPlaceModels;
    private static  ListPlaceAdapter adapter;
    String idGoogle;
    Context context;
    AlertDialog dialog;
    Button location_settings;
    boolean alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = MainActivity.this;
        GpsStatus = false;
        alert = false;
        idGoogle = "";
        dialog = new SpotsDialog(context);
        dialog.setTitle("Please wait");
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
        validateLayout();


    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        validateLayout();
        permissionCheck();
    }

    private void permissionCheck(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if(!alert){
                alert = true;
                //Show explanation message
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                // 2. Chain together various setter methods to set the dialog characteristics
                builder.setMessage("We need you location to be sure you are at the right place")
                        .setTitle("Location requested");

                // Add the buttons
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                PERMISSION_ACCESS_FINE_LOCATION);
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        SwitchNoGPS();
                    }
                });

                AlertDialog permissionDialog = builder.create();
                permissionDialog.setCanceledOnTouchOutside(false);
                permissionDialog.show();
            }

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_FINE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    setApplicationUp();

                }
                else {
                    SwitchNoGPS();
                }
                return;
            }
        }
    }


    private void validateLayout(){

        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (GpsStatus && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            dialog.show();
            setApplicationUp();
        }
        else {
            SwitchNoGPS();
        }
    }

    private void setApplicationUp(){
        setContentView(R.layout.activity_main);
        //Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar_main);
        //setSupportActionBar(myToolbar);


        questionPlace = (TextView) findViewById(R.id.place_question);
        currentPlace = (TextView) findViewById(R.id.current_place);

        selectPlace = (TextView) findViewById(R.id.select_place);
        choiceView = (ListView) findViewById(R.id.list_place);

        yesButton = (Button) findViewById(R.id.yesButton);
        noButton = (Button) findViewById(R.id.noButton);

        ListPlaceModels = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            yesButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button
                    String place = currentPlace.getText().toString();
                    openMenu(place,idGoogle);
                }
            });

            noButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button
                    showPlaceList();
                }
            });
            new MainActivity.GetContacts().execute();
        }
    }

    void openMenu(String place, String id)
    {
        Intent intent = new Intent(this, MenuTypeActivity.class);
        intent.putExtra("currentPlace", place);
        intent.putExtra("idGoogle", id);
        startActivity(intent);
    }

    void SwitchNoGPS(){
        dialog.cancel();
        setContentView(R.layout.activity_no_choice_place);
        TextView headerTitle = (TextView) findViewById(R.id.header_title);
        headerTitle.setText("Location unavailable");
        location_settings = (Button) findViewById(R.id.go_to_location);
        location_settings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(viewIntent);
            }
        });

    }

    void showPlaceList()
    {
        adapter = new ListPlaceAdapter(ListPlaceModels, getApplicationContext());
        choiceView.setAdapter(adapter);
        yesButton.setVisibility(View.GONE);
        noButton.setVisibility(View.GONE);
        questionPlace.setVisibility(View.GONE);
        currentPlace.setVisibility(View.GONE);
        selectPlace.setVisibility(View.VISIBLE);
        choiceView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            /////////////////////////////////////////////////////////////////////////////
            ////////////////////// PERMISSION FOR API > 23
            /////////////////////////////////////////////////////////////////////////////
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle(getString(R.string.permissionTitle));

                // Setting Dialog Message
                alertDialog.setMessage(getString(R.string.permissionMessage));

                // On pressing Settings button
                alertDialog.setPositiveButton(getString(R.string.permissionOk), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                                PERMISSION_ACCESS_FINE_LOCATION);
                    }
                });

                // on pressing cancel button
                alertDialog.setNegativeButton(getString(R.string.permissionCancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }

            float minLikelyhood = 0;
            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                    .getCurrentPlace(mGoogleApiClient, null);
            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {

                @Override
                public void onResult(PlaceLikelihoodBuffer likelyPlaces) {

                    if(likelyPlaces.getStatus().isSuccess()){
                        PlaceLikelihood placeLikelihood = likelyPlaces.get(0);
                        Log.i("Location", String.format("Place '%s' has id: %s",
                                placeLikelihood.getPlace().getName(),
                                placeLikelihood.getPlace().getId()));

                        currentPlace.setText(placeLikelihood.getPlace().getName());
                        idGoogle = placeLikelihood.getPlace().getId();
                        for (PlaceLikelihood place : likelyPlaces) {
                            ListPlaceModels.add(new ListPlaceModel(place.getPlace().getName().toString(), place.getPlace().getId().toString()));
                            Log.i("Location", String.format("Place '%s' has likelihood: %s",
                                    place.getPlace().getName(),
                                    placeLikelihood.getPlace().getId()));
                        }
                        likelyPlaces.release();
                        dialog.dismiss();
                    }
                    else {
                        dialog.dismiss();
                        SwitchNoGPS();
                    }

                }

            });
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            choiceView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ListPlaceModel model = (ListPlaceModel) parent.getItemAtPosition(position);
                    openMenu(model.getPlaceName(),model.getPlaceId());
                }
            });
        }
    }
}