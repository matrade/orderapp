package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-08-10.
 */

public class ListPlaceAdapter extends ArrayAdapter<ListPlaceModel> implements View.OnClickListener{

    private ArrayList<ListPlaceModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView placeName;
        String placeId;
    }

    public ListPlaceAdapter(ArrayList<ListPlaceModel> data, Context context) {
        super(context, R.layout.choice_place_cell, data);
        this.dataSet = data;
        this.mContext=context;
    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        ListPlaceModel dataModel=(ListPlaceModel) object;

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ListPlaceModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.choice_place_cell, parent, false);
            viewHolder.placeName = (TextView) convertView.findViewById(R.id.place_name);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.placeName.setText(dataModel.getPlaceName());
        viewHolder.placeId = dataModel.getPlaceId();
        // Return the completed view to render on screen
        return convertView;
    }
}
