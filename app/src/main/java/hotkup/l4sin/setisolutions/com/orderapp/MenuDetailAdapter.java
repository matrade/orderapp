package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-08-07.
 */

public class MenuDetailAdapter extends ArrayAdapter<MenuDetailModel> implements View.OnClickListener{

    private ArrayList<MenuDetailModel> dataSet;
    Context mContext;
    TextView totalPrice;
    float basicPrice;
    RadioButton selected;

    // View lookup cache
    private static class ViewHolder {
        TextView menuDetailName;
        TextView menuDetailPrice;
        RadioButton optionChoose;
        TextView currency;
    }

    public MenuDetailAdapter(ArrayList<MenuDetailModel> data, Context context, TextView detailPrice) {
        super(context, R.layout.menu_detail_cell, data);
        this.dataSet = data;
        this.mContext=context;
        totalPrice = detailPrice;

        String basicConverter = totalPrice.getText().toString();
        //convert coma float by dot float
        basicConverter = basicConverter.replace(',','.');
        basicPrice = Float.parseFloat(basicConverter);
    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        MenuDetailModel dataModel=(MenuDetailModel) object;
        switch (v.getId())
        {
            case R.id.option_select_button:
                //Variables.addOnList.add(object.toString());
                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MenuDetailModel dataModel = getItem(position);
        final ViewHolder viewHolder; // view lookup cache stored in tag
        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.menu_detail_cell, parent, false);
            viewHolder.menuDetailName = (TextView) convertView.findViewById(R.id.option_name);
            viewHolder.menuDetailPrice = (TextView) convertView.findViewById(R.id.option_price);
            viewHolder.optionChoose = (RadioButton) convertView.findViewById(R.id.option_select_button);
            viewHolder.currency = (TextView) convertView.findViewById(R.id.food_currency);
            result=convertView;
            convertView.setTag(viewHolder);

        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }


        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.menuDetailName.setText(dataModel.getDetailName());

        if (dataModel.getDetailPrice() != null) {
            viewHolder.currency.setText(Variables.currency);
            viewHolder.menuDetailPrice.setText(String.format("%.2f", Float.parseFloat(dataModel.getDetailPrice())));
        }
        else {
            viewHolder.menuDetailPrice.setVisibility(View.GONE);
            viewHolder.currency.setVisibility(View.GONE);
        }

        if (position == 0){
            viewHolder.optionChoose.setChecked(true);
            Variables.option = dataModel.getDetailName();
            selected = viewHolder.optionChoose;
            if(dataModel.getDetailPrice() != null) {
                float floatCurrentPrice = basicPrice;
                float floatOptionPrice = Float.parseFloat(dataModel.getDetailPrice());
                floatCurrentPrice += floatOptionPrice;
                //convert the string to the format with the coma
                totalPrice.setText(String.format("%.2f", floatCurrentPrice));
            }
        }

        viewHolder.optionChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected.setChecked(false);
                Variables.option = dataModel.getDetailName();
                Float totalPriceContent = basicPrice;
                if(dataModel.getDetailPrice() != null){
                    //value of the option
                    float floatOptionPrice = Float.parseFloat(dataModel.getDetailPrice());
                    //update variable global
                    Variables.optionsPrice = floatOptionPrice;
                    //current value to update (basic + option)
                    totalPriceContent += floatOptionPrice;
                }
                else{
                    Variables.optionsPrice = 0;
                }

                for (MenuExtraModel addonItem : Variables.addOnList){
                    totalPriceContent += Float.parseFloat(addonItem.getExtraPrice());
                }

                totalPrice.setText(String.format("%.2f",totalPriceContent));

                Log.d("radio",dataModel.getDetailName());
                viewHolder.optionChoose.setChecked(true);
                selected = viewHolder.optionChoose;
            }
        });
        return convertView;
    }
}
