package hotkup.l4sin.setisolutions.com.orderapp;

/**
 * Created by malicktraoredermane on 17-09-30.
 */

public class MenuExtraModel {
    String detailName;
    String detailPrice;

    public MenuExtraModel(String name){
        detailName = name;
        detailPrice = "0";
    }

    public MenuExtraModel(String name, String price){
        detailName = name;
        detailPrice = price;
    }

    String getExtraName(){
        return detailName;
    }

    String getExtraPrice(){
        return detailPrice;
    }
}
