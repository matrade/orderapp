package hotkup.l4sin.setisolutions.com.orderapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

/**
 * Created by malicktraoredermane on 17-08-11.
 */

public class MenuTypeActivity extends AppCompatActivity {
    ArrayList<MenuTypeModel> MenuTypeModels;
    ListView listView;
    TextView place;
    private static MenuTypeAdapter adapter;
    private String TAG = MainActivity.class.getSimpleName();
    private static String url = "http://hkmanager.com/orderapp/menu/m_get_menu.php";
    View cartButton;
    String id;
    String placeName;
    Context context;
    AlertDialog dialog;
    TextView cartContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_type);
        cartButton = (View) findViewById(R.id.cart_button_main);
        listView = (ListView) findViewById(R.id.list_type_food);
        place = (TextView) findViewById(R.id.place_name);
        TextView title = (TextView) findViewById(R.id.header_title);
        title.setText("Select a type of meal");

        cartContent = (TextView) findViewById(R.id.cart_content);
        cartContent.setText(String.valueOf(Variables.cartContent.size()));
        context = MenuTypeActivity.this;

        dialog = new SpotsDialog(context);
        dialog.setTitle("Please wait");
        dialog.show();

        Intent intent = getIntent();
        id = intent.getStringExtra("idGoogle");

        MenuTypeModels= new ArrayList<>();
        cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openCart();
            }
        });
        new GetContacts().execute();
    }

    @Override
    public void onResume(){
        super.onResume();
        cartContent.setText(String.valueOf(Variables.cartContent.size()));
    }

    void changeLayout(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.activity_no_menu_type);
                TextView headertitle = (TextView) findViewById(R.id.header_title);
                headertitle.setText("Menu");
                Button backToPlace = (Button) findViewById(R.id.cancel_place);
                backToPlace.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });

            }
        });
    }

    void openCart()
    {
        dialog.dismiss();
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url,"idGoogle", id);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    Variables.menuRestaurant = jsonObj;

                    // Getting JSON Array node
                    JSONArray categories = jsonObj.getJSONArray("type");
                    placeName = jsonObj.getString("name");
                    Variables.currency = jsonObj.getString("currency");
                    Variables.placeNameHolder = placeName;
                    if(placeName.length()==0){
                        changeLayout();
                    }
                    // looping through All Contacts
                    for (int i = 0; i < categories.length(); i++) {
                        JSONObject type = categories.getJSONObject(i);
                        String name = type.getString("name");
                        MenuTypeModels.add(new MenuTypeModel(name));
                    }
                }
                catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            dialog.dismiss();
            adapter = new MenuTypeAdapter(MenuTypeModels, getApplicationContext());

            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    MenuTypeModel typeSelected = MenuTypeModels.get(position);
                    String typeName = MenuTypeModels.get(position).getMenuType();
                    showMenu(position, typeName);
                }
            });
            place.setText(placeName);
        }
    }

    void showMenu(int pos, String typeName){
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("position", pos);
        intent.putExtra("typeName",typeName);
        intent.putExtra("placeName",placeName);
        startActivity(intent);
    }
}
