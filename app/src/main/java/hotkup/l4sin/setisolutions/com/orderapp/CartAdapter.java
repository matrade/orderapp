package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-08-11.
 */

public class CartAdapter extends ArrayAdapter<CartModel> implements View.OnClickListener{

    private ArrayList<CartModel> dataSet;
    Context mContext;
    TextView totalCartValue;

    // View lookup cache
    private static class ViewHolder {
        TextView cartName;
        TextView cartOptionName;
        TextView cartAddOnName;
        ImageButton cartItemRemove;
        float cartPrice;
    }

    public CartAdapter(ArrayList<CartModel> data, Context context,TextView value) {
        super(context, R.layout.cart_cell, data);
        this.dataSet = data;
        this.mContext=context;
        this.totalCartValue = value;
    }

    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        CartModel dataModel=(CartModel) object;
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final CartModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        final CartAdapter.ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new CartAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.cart_cell, parent, false);
            viewHolder.cartName = (TextView) convertView.findViewById(R.id.cart_plat_name);
            viewHolder.cartOptionName = (TextView) convertView.findViewById(R.id.option_detail);
            viewHolder.cartItemRemove = (ImageButton) convertView.findViewById(R.id.cart_plat_option);
            viewHolder.cartAddOnName = (TextView) convertView.findViewById(R.id.addon_detail);
            viewHolder.cartItemRemove.setTag(position);
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CartAdapter.ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.cartName.setText(dataModel.getCartName());
        viewHolder.cartOptionName.setText(dataModel.getCartDetailName());
        viewHolder.cartPrice = dataModel.getCartPrice();
        viewHolder.cartItemRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int positionToRemove = (int)v.getTag(); //get the position of the view to delete stored in the tag
                removeItem(positionToRemove, viewHolder.cartPrice);
                removeFromCart(dataModel.getCartName(),dataModel.getCartPrice(),dataModel.getCartDetailName());
                notifyDataSetChanged(); //remove the item
            }
        });
        String addonValue = getRawAddonValue(dataModel.getAddonListContent());
        viewHolder.cartAddOnName.setText(addonValue);
        if(dataModel.getCartDetailName() == ""){
            viewHolder.cartOptionName.setVisibility(View.GONE);
        }
        if(addonValue == ""){
            viewHolder.cartAddOnName.setVisibility(View.GONE);
        }
        // Return the completed view to render on screen
        return convertView;
    }

    private String getRawAddonValue(ArrayList<MenuExtraModel> arrayValue){
        String returnValue = "";
        for(MenuExtraModel value : arrayValue){
            returnValue += value.getExtraName();
            returnValue += "/";
        }
        if(returnValue.length() > 0) {
            returnValue = returnValue.substring(0, returnValue.length() - 1);
        }
        return returnValue;
    }

    private void removeFromCart(String namePlat, float pricePlat, String optionPlat){
        for(Order content : Variables.cartContent){
            if(content.getPlatName() == namePlat && content.getPlatPrice() == pricePlat
                    && content.getOption() == optionPlat){
                Variables.cartContent.remove(content);
                break;
            }
        }
    }

    public void removeItem(int position, float price){
        //convert array to ArrayList, delete item and convert back to array
        dataSet.remove(position);
        notifyDataSetChanged(); //refresh your listview based on new data
        String currentValueConvert = totalCartValue.getText().toString();
        currentValueConvert = currentValueConvert.replace(',','.');
        float currentValue = Float.parseFloat(currentValueConvert);
        currentValue -= price;
        totalCartValue.setText(String.format("%.2f",currentValue));
    }
}
