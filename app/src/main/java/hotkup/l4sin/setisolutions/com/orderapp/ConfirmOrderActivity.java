package hotkup.l4sin.setisolutions.com.orderapp;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;

import static hotkup.l4sin.setisolutions.com.orderapp.barcode.BarcodeCaptureActivity.BarcodeObject;

/**
 * Created by malicktraoredermane on 17-09-27.
 */

public class ConfirmOrderActivity extends AppCompatActivity {
    String barecodeResult;
    Button confirmOrder;
    Context context;
    AlertDialog dialog;
    private static String url_barecode = "http://hkmanager.com/orderapp/menu/m_check_barecode.php";
    private static String url_order = "http://hkmanager.com/orderapp/menu/m_send_order.php";
    Button backToCart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        barecodeResult = "";
        context = ConfirmOrderActivity.this;
        dialog = new SpotsDialog(context);
        dialog.setTitle("Please wait");

        setContentView(R.layout.activity_confirmation_order);
        confirmOrder = (Button) findViewById(R.id.confirm_order);
        TextView headertitle = (TextView) findViewById(R.id.header_title);
        headertitle.setText("Prepare your order");
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                barecodeResult= "";
            } else {
                barecodeResult = extras.getString(BarcodeObject);
            }
        } else {
            barecodeResult = (String) savedInstanceState.getSerializable(BarcodeObject);
        }
        /*if(barecodeResult != "" && barecodeResult != null)
        {
            saveScanList();
        }*/
        Log.d("QRCode",barecodeResult);
        confirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetOrderConfirmation().execute();
            }
        });

        if (!Variables.qrcodeAccepted) {
            new GetBareCodeConfirmation().execute();
        }
    }

    private void endActivity(){
        finish();
    }

    private void saveScanList(){
        Timestamp timeSet = new Timestamp(System.currentTimeMillis());
        Variables.timestamp = timeSet;
    }

    private void changeLayout(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Variables.isQrcodeScanned = false;
                setContentView(R.layout.activity_no_confirmation_order);
                TextView headertitle = (TextView) findViewById(R.id.header_title);
                headertitle.setText("Prepare your order");
                backToCart = (Button) findViewById(R.id.retry_order);
                backToCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }
        });
    }


    private class GetBareCodeConfirmation extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url_barecode and getting response
            String jsonStr = sh.makeServiceCall(url_barecode, "barcode",barecodeResult);

            if(!Variables.isQrcodeScanned || jsonStr.length() < 5 || !jsonStr.contains(Variables.placeNameHolder)){
                changeLayout();
            }
            else if (jsonStr != null) {
                Log.d("order",jsonStr);
                saveScanList();
            } else {
                Log.e("order", "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }
            return null;
        }
    }

    private class GetOrderConfirmation extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            ArrayList<Order> orderRecap = Variables.cartContent;

            JSONArray orderToSend = new JSONArray();
            for (Order order : orderRecap){
                JSONObject inputOrder = new JSONObject();
                try {
                    inputOrder.put("name",order.getPlatName());
                    inputOrder.put("price",order.getPlatPrice());
                    inputOrder.put("option",order.getOption());
                    JSONArray extraList = new JSONArray();
                    for (MenuExtraModel extra : order.getExtra()){
                        extraList.put(extra.getExtraName());
                    }
                    inputOrder.put("addon",extraList);
                    orderToSend.put(inputOrder);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // Making a request to url_barecode and getting response
            String jsonStr = sh.makeServiceCall(url_order, "order",orderToSend.toString());

            Log.i("order validation", jsonStr);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Variables.cartContent.clear();
            finish();
        }
    }


}
