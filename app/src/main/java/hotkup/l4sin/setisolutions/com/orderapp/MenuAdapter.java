package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-08-06.
 */

public class MenuAdapter extends ArrayAdapter<MenuModel> implements View.OnClickListener{

    private ArrayList<MenuModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView menuName;
        TextView menuPrice;
        TextView currency;
    }

    public MenuAdapter(ArrayList<MenuModel> data, Context context) {
        super(context, R.layout.menu_cell, data);
        this.dataSet = data;
        this.mContext=context;
    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        MenuModel dataModel=(MenuModel) object;

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        MenuModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.menu_cell, parent, false);
            viewHolder.menuName = (TextView) convertView.findViewById(R.id.food_name);
            viewHolder.menuPrice = (TextView) convertView.findViewById(R.id.food_price);
            viewHolder.currency = (TextView) convertView.findViewById(R.id.food_currency);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.menuName.setText(dataModel.getMenuName());
        viewHolder.menuPrice.setText(String.format("%.2f",Float.parseFloat(dataModel.getMenuPrice())));
        viewHolder.currency.setText(Variables.currency);
        // Return the completed view to render on screen
        return convertView;
    }
}
