package hotkup.l4sin.setisolutions.com.orderapp;

/**
 * Created by malicktraoredermane on 17-08-07.
 */

public class MenuDetailModel {
    String detailName;
    String detailPrice;

    public MenuDetailModel(String name){
        detailName = name;
    }

    public MenuDetailModel(String name, String price){
        detailName = name;
        detailPrice = price;
    }

    String getDetailName(){
        return detailName;
    }

    String getDetailPrice(){
        return detailPrice;
    }
}
