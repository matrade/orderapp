package hotkup.l4sin.setisolutions.com.orderapp;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-08-13.
 */

public class Order {
    String platName;
    String option;
    ArrayList<MenuExtraModel> extraList;
    float platPrice;

    public Order(String name, float price, ArrayList param){
        platName = name;
        platPrice = price;
        option = "";
        extraList = new ArrayList<>();
        extraList = param;
    }

    public Order(String name, String garniture1, float price , ArrayList param){
        platName = name;
        option = garniture1;
        platPrice = price;
        extraList = new ArrayList<>();
        extraList = param;
    }

    public String getPlatName(){
        return platName;
    }

    public String getOption(){
        return option;
    }

    public float getPlatPrice(){
        return platPrice;
    }

    public ArrayList<MenuExtraModel> getExtra(){
        return extraList;
    }



}
