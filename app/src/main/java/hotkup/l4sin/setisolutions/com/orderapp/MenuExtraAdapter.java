package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-09-30.
 */

public class MenuExtraAdapter extends ArrayAdapter<MenuExtraModel> implements View.OnClickListener {

    private ArrayList<MenuExtraModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView menuExtraName;
        TextView menuExtraPrice;
        CheckBox extraChoose;
        TextView currency;
    }

    public MenuExtraAdapter(ArrayList<MenuExtraModel> data, Context context) {
        super(context, R.layout.menu_extra_cell, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        MenuExtraModel dataModel = (MenuExtraModel) object;
        switch (v.getId()) {
            case R.id.extra_select_button:
                //Variables.addOnList.add(object);
                break;
        }
    }

    private int lastPosition = -1;

    private boolean parseRemoveFromAddonList(Boolean action, MenuExtraModel data){
        boolean returnValue = false;
        for(MenuExtraModel elem : Variables.addOnList) {
            String elemValue = elem.getExtraName();
            if (elemValue == data.getExtraName()) {
                if(action){
                    returnValue = true;
                    break;
                }
                else{
                    Variables.addOnList.remove(elem);
                    break;
                }
            }
        }
        return returnValue;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MenuExtraModel dataModel = getItem(position);
        final MenuExtraAdapter.ViewHolder viewHolder; // view lookup cache stored in tag
        final View result;

        if (convertView == null) {

            viewHolder = new MenuExtraAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.menu_extra_cell, parent, false);
            viewHolder.menuExtraName = (TextView) convertView.findViewById(R.id.extra_name);
            viewHolder.menuExtraPrice = (TextView) convertView.findViewById(R.id.extra_price);
            viewHolder.extraChoose = (CheckBox) convertView.findViewById(R.id.extra_select_button);
            viewHolder.currency = (TextView) convertView.findViewById(R.id.food_currency);
            
            result = convertView;
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (MenuExtraAdapter.ViewHolder) convertView.getTag();
            result = convertView;
        }

        if(parseRemoveFromAddonList(true,dataModel)){
            viewHolder.extraChoose.setChecked(true);
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.menuExtraName.setText(dataModel.getExtraName());
        viewHolder.menuExtraPrice.setText(String.format("%.2f",Float.parseFloat(dataModel.getExtraPrice())));
        viewHolder.currency.setText(Variables.currency);

        viewHolder.extraChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.extraChoose.setChecked(viewHolder.extraChoose.isChecked());
                if (viewHolder.extraChoose.isChecked()){
                    Variables.addOnList.add(dataModel);
                }

                else{
                    parseRemoveFromAddonList(false,dataModel);
                }
            }
        });
        return convertView;
    }
}
