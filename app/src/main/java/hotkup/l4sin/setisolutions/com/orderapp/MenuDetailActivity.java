package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-08-07.
 */

public class MenuDetailActivity extends AppCompatActivity {

    private static final int EXTRA_REQUEST_CODE = 1;
    public static final String EXTRA_TAG_CODE = "Extra";

    ArrayList<MenuDetailModel> MenuDetailModels;
    ListView listView;
    TextView platName;
    TextView platPrice;
    View addToCart;
    View cartButton;
    ImageButton goToExtra;
    TextView platDescription;
    TextView cartContent;
    LinearLayout bottomExtraLayout;
    String description;
    String garniture;
    TextView currency;
    Integer positionType;
    Integer positionMenu;
    String name;
    String price;
    boolean toogle;
    int sizeExtra;
    float initialPrice;
    private static MenuDetailAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu_details);
        Variables.addOnList.clear();
        initialPrice = 0;
        listView = (ListView) findViewById(R.id.option_list);
        platName = (TextView) findViewById(R.id.header_title);
        platPrice = (TextView) findViewById(R.id.plat_price);

        addToCart = (View) findViewById(R.id.option_confirm);
        goToExtra = (ImageButton) findViewById(R.id.go_to_extra);
        platDescription = (TextView) findViewById(R.id.plat_description);
        bottomExtraLayout = (LinearLayout) findViewById(R.id.botom_extra_layout);
        currency = (TextView) findViewById(R.id.food_currency);
        currency.setText(Variables.currency);
        cartContent = (TextView) findViewById(R.id.cart_content);
        cartContent.setText(String.valueOf(Variables.cartContent.size()));
        toogle = true;
        sizeExtra = 0;

        MenuDetailModels= new ArrayList<>();
        cartButton = (View) findViewById(R.id.cart_button_main);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                positionType = null;
                positionMenu = null;
            } else {
                positionType = extras.getInt("type");
                positionMenu = extras.getInt("detail");
            }
        } else {
            positionType = (Integer) savedInstanceState.getSerializable("type");
            positionMenu = (Integer) savedInstanceState.getSerializable("detail");
        }

        cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openCart();
            }
        });

        goToExtra.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openExtra();
            }
        });

        new MenuDetailActivity.GetContacts().execute();

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String option = Variables.option;
                ArrayList <MenuExtraModel> addon = new ArrayList<>(Variables.addOnList);

                Order order = new Order(name, option,Float.parseFloat(price),addon);
                Variables.cartContent.add(order);

                cartContent.setText(String.valueOf(Variables.cartContent.size()));

                Toast.makeText(getApplicationContext(), "Added successfully!",
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        cartContent.setText(String.valueOf(Variables.cartContent.size()));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EXTRA_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                Log.d("Extra", "success");
                float resultAddon = 0;
                for (MenuExtraModel elem : Variables.addOnList){
                    String stringValue = elem.getExtraPrice();
                    Float floatValue = Float.parseFloat(stringValue);
                    resultAddon += floatValue;
                }
                Float optionPriceLocal = Variables.optionsPrice;
                resultAddon += initialPrice + optionPriceLocal;
                //convert the string to the format with the coma
                platPrice.setText(String.format("%.2f",resultAddon));
                price = Float.toString(resultAddon);
            }
        }
    }

    void openCart() {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }

    void openExtra(){
        Intent intent = new Intent(getApplicationContext(), MenuExtraActivity.class);
        intent.putExtra("detail",positionMenu);
        intent.putExtra("type", positionType);
        startActivityForResult(intent,EXTRA_REQUEST_CODE);
    }

    void hideExtra(){
        bottomExtraLayout.setVisibility(View.GONE);
    }



    /**
     * Async task class to get json by making HTTP call
     */
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            JSONObject menuList = Variables.menuRestaurant;
            try {
                JSONArray categories = menuList.getJSONArray("type");
                JSONObject type = categories.getJSONObject(positionType);
                JSONArray menuAll = type.getJSONArray("meal");
                JSONObject menu = menuAll.getJSONObject(positionMenu);
                Boolean option_flag = menu.getBoolean("option_flag");
                name = menu.getString("name");
                price = menu.getString("price");

                TextView title = (TextView) findViewById(R.id.header_title);
                title.setText(name);

                description = menu.getString("description");
                if (description.length() == 0){
                    description = "The description of this product is not avaible";
                }
                if(option_flag) {
                    JSONArray optionDish = menu.getJSONArray("option");
                    for (int i = 0; i < optionDish.length(); i++) {

                        JSONObject option = optionDish.optJSONObject(i);
                        String optionPrice = "";
                        String rawPrice = option.getString("price");
                        float intPrice = Float.parseFloat(rawPrice);

                        if (intPrice > 0) {
                            optionPrice += rawPrice;
                        }

                        String optionName = option.getString("name");

                        if (optionPrice == "") {
                            MenuDetailModels.add(new MenuDetailModel(optionName));
                        } else {
                            MenuDetailModels.add(new MenuDetailModel(optionName, optionPrice));
                        }
                    }
                }

                sizeExtra = 0;

                Boolean addon_flag = menu.getBoolean("addon_flag");
                if (addon_flag){
                    JSONArray addons = menu.getJSONArray("addOn");
                    sizeExtra = addons.length();
                }

                if (!addon_flag){
                    hideExtra();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        platName.setText(name);
                        //convert the string to the format with the coma
                        platPrice.setText(String.format("%.2f",Float.parseFloat(price)));
                        platDescription.setText(description);
                        initialPrice = Float.parseFloat(price);
                    }
                });

            }

            catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        public void updatePrice(float value){
            float rawCurrentPrice = Float.parseFloat(price);
            rawCurrentPrice += value;
            price = Float.toString(rawCurrentPrice);
            //platPrice.setText(price);
        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            adapter = new MenuDetailAdapter(MenuDetailModels, getApplicationContext(),platPrice);

            listView.setAdapter(adapter);
        }
    }

}

