package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-06-28.
 */

public class MenuActivity extends AppCompatActivity{

    ArrayList<MenuModel> MenuModels;
    ListView listView;
    TextView menuType;
    //TextView place;
    //String placeName;
    private static MenuAdapter adapter;
    Integer position;
    View cartButton;
    TextView cartContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        String typeName = "";
        Variables.option = "";
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                position= null;
            } else {
                position= extras.getInt("position");
                typeName = extras.getString("typeName");
            }
        }
        else {
            position= (Integer) savedInstanceState.getSerializable("position");
            typeName = (String) savedInstanceState.getSerializable("name");
        }


        menuType = (TextView) findViewById(R.id.menu_type);
        menuType.setText(typeName);

        TextView title = (TextView) findViewById(R.id.header_title);
        title.setText("Select a meal");

        listView = (ListView) findViewById(R.id.list_food);
        MenuModels= new ArrayList<>();
        cartButton = (View) findViewById(R.id.cart_button_main);

        cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openCart();
            }
        });
        new MenuActivity.GetContacts().execute();

        cartContent = (TextView) findViewById(R.id.cart_content);
        cartContent.setText(String.valueOf(Variables.cartContent.size()));
    }

    @Override
    public void onResume(){
        super.onResume();
        // put your code here...
        cartContent.setText(String.valueOf(Variables.cartContent.size()));
    }

    void openDetails(int positionDetail)
    {
        Intent intent = new Intent(this, MenuDetailActivity.class);
        intent.putExtra("detail",positionDetail);
        intent.putExtra("type", position);
        startActivity(intent);
    }

    void openCart()
    {
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            JSONObject menuList = Variables.menuRestaurant;
            try {
                JSONArray categories = menuList.getJSONArray("type");
                JSONObject type = categories.getJSONObject(position);
                JSONArray menuAll = type.getJSONArray("meal");
                for (int i = 0; i < menuAll.length(); i++) {
                    JSONObject menu = menuAll.getJSONObject(i);
                    String menuName = menu.getString("name");
                    String menuPrice = menu.getString("price");
                    MenuModels.add(new MenuModel(menuName, menuPrice));
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            adapter = new MenuAdapter(MenuModels, getApplicationContext());

            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    MenuModel typeSelected = MenuModels.get(position);
                    openDetails(position);
                }
            });
        }
    }


}
