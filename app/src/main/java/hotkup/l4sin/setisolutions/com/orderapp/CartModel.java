package hotkup.l4sin.setisolutions.com.orderapp;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-08-11.
 */

public class CartModel {

    String cartName;
    String cartDetailName;
    float cartPrice;
    ArrayList<MenuExtraModel> addonListContent;

    public CartModel(String name, float price, ArrayList<MenuExtraModel> list){
        cartName = name;
        cartDetailName = "";
        cartPrice = price;
        addonListContent = new ArrayList<>();
        addonListContent = list;
    }

    public CartModel(String name, String detailName, float price, ArrayList<MenuExtraModel> list){
        cartName = name;
        cartDetailName = detailName;
        cartPrice = price;
        addonListContent = new ArrayList<>();
        addonListContent = list;
    }

    String getCartName(){
        return cartName;
    }

    String getCartDetailName(){
        return cartDetailName;
    }

    ArrayList getAddonListContent(){
        return addonListContent;
    }

    float getCartPrice(){return  cartPrice;}
}
