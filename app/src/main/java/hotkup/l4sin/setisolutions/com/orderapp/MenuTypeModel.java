package hotkup.l4sin.setisolutions.com.orderapp;

/**
 * Created by malicktraoredermane on 17-08-11.
 */

public class MenuTypeModel {
    String menuType;

    public MenuTypeModel(String type){
        menuType = type;
    }

    public String getMenuType(){
        return menuType;
    }
}
