package hotkup.l4sin.setisolutions.com.orderapp;

/**
 * Created by malicktraoredermane on 17-08-10.
 */

public class ListPlaceModel {
    String placeName;
    String placeId;

    public ListPlaceModel(String name, String id){
        placeName = name;
        placeId = id;
    }

    String getPlaceName(){
        return placeName;
    }

    String getPlaceId(){
        return placeId;
    }
}
