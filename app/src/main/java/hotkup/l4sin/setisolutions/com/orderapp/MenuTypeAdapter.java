package hotkup.l4sin.setisolutions.com.orderapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-08-11.
 */

public class MenuTypeAdapter extends ArrayAdapter<MenuTypeModel> implements View.OnClickListener {

    private ArrayList<MenuTypeModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView menuTypeName;
    }

    public MenuTypeAdapter(ArrayList<MenuTypeModel> data, Context context) {
        super(context, R.layout.menu_type_cell, data);
        this.dataSet = data;
        this.mContext = context;

    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        MenuTypeModel dataModel = (MenuTypeModel) object;

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MenuTypeModel dataModel = getItem(position);
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.menu_type_cell, parent, false);
            viewHolder.menuTypeName = (TextView) convertView.findViewById(R.id.type_food_name);
            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.menuTypeName.setText(dataModel.getMenuType());
        return convertView;
    }
}