package hotkup.l4sin.setisolutions.com.orderapp;

import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * Created by malicktraoredermane on 17-08-13.
 */

public class Variables {
        public static JSONObject menuRestaurant = null;
        public static float invoice = 0;
        public static ArrayList<Order> cartContent = new ArrayList<>();
        public static ArrayList<MenuExtraModel> addOnList = new ArrayList<>();
        public static String option = "";
        public static float optionsPrice = 0;
        public static Timestamp timestamp;
        public static String placeNameHolder = "";
        public static String currency = "%";
        public static boolean qrcodeAccepted = false;
        public static boolean isQrcodeScanned = false;
}
